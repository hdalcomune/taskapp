package com.avenuecode.taskapp;

import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {

  private TaskRepository taskRepository;

  public TaskController(TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @GetMapping(path = "/taskapp/task/{id}")
  public ResponseEntity<Optional<Task>> getTask(@PathVariable("id") Long id) {
    return ResponseEntity.ok(taskRepository.findById(id));
  }

  @GetMapping(path = "/taskapp/tasks")
  public ResponseEntity<Iterable<Task>> getTasks() {
    return ResponseEntity.ok(taskRepository.findAll());
  }

  @PostMapping(path = "/taskapp/task")
  public ResponseEntity<Task> createTask(@RequestBody Task task) {
    Task savedTask = this.taskRepository.save(task);
    return ResponseEntity.status(201).body(savedTask);
  }
}

# Instructions

## Executing the project

To start this project execute the main class `com.avenuecode.taskapp.TaskApplication`. 
The app will start in port 8080.

## Using it

To use try the Postman collection added to this project. 
Install Postman and play a little with it to see how it works.    